import XMonad

import XMonad.StackSet;

import XMonad.Util.EZConfig
import XMonad.Util.Cursor
import XMonad.Util.Loggers

import XMonad.Operations (unGrab)

import XMonad.Layout.ThreeColumns
import XMonad.Layout.Magnifier
import XMonad.Layout.SimpleFloat

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.OnPropertyChange
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

customLayout = tiled ||| Mirror tiled ||| Full ||| threeCol ||| simpleFloat
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled = Tall nmaster delta ratio
    nmaster = 1
    ratio = 1/2
    delta = 3/100

manageZoomHook =
  composeAll
    [ (className =? zoomClassName) <&&> shouldFloat <$> title --> doFloat
    , (className =? zoomClassName) <&&> shouldSink <$> title --> doSink
    ]
  where
    zoomClassName = "zoom"
    tileTitles =
      [ "Zoom - Free Account" -- main window
      , "Zoom - Licensed Account" -- main window
      , "Zoom" -- meeting window on creation
      , "Zoom Meeting" -- meeting window shortly after creation
      ]
    shouldFloat title = title `notElem` tileTitles
    shouldSink title = title `elem` tileTitles
    doSink = (ask >>= doF . sink) <+> doF swapDown

-- SDL2 does not define whatever property GLFW adds to cause windows to
-- automatically float (my guess is that it's the maximum window size), so I am
-- manually renaming the window and catching it here.
manageOpenGl =
  title =? "Game" --> doFloat

myManageHook =
  manageZoomHook
    <+> manageDocks
    <+> manageOpenGl
    <+> manageHook def

myHandleEventHook =
  mconcat
    [ onTitleChange manageZoomHook
    , handleEventHook def
    ]

configuration = def
  { modMask = mod4Mask
  , layoutHook = customLayout
  , terminal = "kitty"
  , focusFollowsMouse = False
  , manageHook = myManageHook
  , startupHook = setWMName "LG3D"
  }
  `additionalKeysP`
  [ ("M-S-z", spawn "xscreensaver-command -lock")
  , ("M-S-=", unGrab *> spawn "scrot -s")
  , ("M-]", spawn "brave")
  ]

customXmobarPP :: PP
customXmobarPP = def
  { ppSep = magenta " • "
  , ppTitleSanitize = xmobarStrip
  , ppCurrent = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
  , ppHidden = white . wrap " " ""
  , ppHiddenNoWindows = lowWhite . wrap " " ""
  , ppUrgent = red . wrap (yellow "!") (yellow "!")
  , ppOrder = \[ws, l, _, wins] -> [ws, l, wins]
  , ppExtras = [logTitles formatFocused formatUnfocused]
  }
    where
      formatFocused = wrap (white "[") (white "]") . magenta . ppWindow
      formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue . ppWindow

      ppWindow :: String -> String
      ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

      blue, lowWhite, magenta, red, white, yellow :: String -> String
      magenta  = xmobarColor "#ff79c6" ""
      blue     = xmobarColor "#bd93f9" ""
      white    = xmobarColor "#f8f8f2" ""
      yellow   = xmobarColor "#f1fa8c" ""
      red      = xmobarColor "#ff5555" ""
      lowWhite = xmobarColor "#bbbbbb" ""

main :: IO ()
main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB (statusBarProp "xmobar" (pure customXmobarPP)) defToggleStrutsKey
     $ configuration
