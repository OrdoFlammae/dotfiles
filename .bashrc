#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

HISTCONTROL=ignorespace:ignoredups
HISTFILESIZE=100000
HISTIGNORE='exit:ls:history:clear'
HISTSIZE=10000

alias ls='eza -lah --color=auto'
alias grep='grep --color=auto'
alias scrot='scrot --freeze'
alias yt-dlp='yt-dlp -f "b[height<=720]/bv*[height<=720]+ba*" --embed-metadata'

# Kitty aliases
if [ "$TERM" = "xterm-kitty" ]; then
    alias icat='kitty +kitten icat'
    alias ssh='kitty +kitten ssh'
fi

PS1='[\u@\h \W]\$ '

[ -f "$HOME/.bashrc.local" ] && source "$HOME/.bashrc.local"
[ -f "$HOME/.pathrc" ] && source "$HOME/.pathrc"
