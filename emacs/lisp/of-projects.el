(use-package project-tasks
  :ensure (:host github :repo "TxGVNN/project-tasks" :files ("*.el"))
  :commands (project-tasks)

  :init
  (add-to-list 'project-switch-commands '(project-tasks "Tasks") t)
  (with-eval-after-load 'embark
    (define-key embark-file-map (kbd "P") #'project-tasks-in-dir))

  :custom
  (project-tasks-file "tasks.org")
  (project-tasks-root-func #'project-tasks-project-root)

  :bind
  (:map project-prefix-map
        ("P" . project-tasks)
        ("o" . project-tasks-capture)
        ("O" . project-tasks-jump)))

(provide 'of-projects)
