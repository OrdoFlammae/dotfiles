(require 'of-packages)

(use-package pdf-tools
  :defer t
  :config
  (pdf-tools-install))

(provide 'of-pdf)
