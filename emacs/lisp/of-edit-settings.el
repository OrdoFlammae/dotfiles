(set-default-coding-systems 'utf-8) ;; Mostly necessary for Windows

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

(global-auto-revert-mode 1)

(add-hook 'before-save-hook #'whitespace-cleanup)

(add-hook 'org-mode-hook #'electric-quote-local-mode)

(require 'of-packages)

(use-package hungry-delete
  :config
  (setq hungry-delete-join-reluctantly t)
  (global-hungry-delete-mode 1))

(provide 'of-edit-settings)
