(require 'of-packages)
(require 'of-general)

(use-package bluetooth
  :after (general)
  :config
  (of/global-definer
    "ub" 'bluetooth-list-devices))

(provide 'of-os-utils)
