(require 'of-packages)

(add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-ts-mode))

(use-package cmake-mode)
(use-package cmake-font-lock
  :after cmake-mode)

(use-package ssh-config-mode)
(use-package ledger-mode)
(use-package cobol-mode)
(use-package glsl-mode)
(use-package nix-mode
  :mode "\\.nix\\'")

(provide 'of-generic-modes)
