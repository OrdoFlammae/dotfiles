(require 'of-packages)
(require 'of-general)

(setq auto-revert-check-vc-info t)

(use-package magit
  :after (general project)
  :commands (magit-clone magit-init)

  :init
  (define-key project-prefix-map (kbd "m") 'magit-project-status)
  (add-to-list 'project-switch-commands '(magit-project-status "Magit") t)

  (of/global-definer
    "m" '(:prefix-command magit-commands-map :which-key "magit")
    "mc" 'magit-clone
    "mi" 'magit-init))

(provide 'of-magit)
