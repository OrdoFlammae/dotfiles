(require 'of-packages)
(require 'of-general)

(use-package elfeed
  :after general
  :config
  ;; There are some feeds I am specifically subscribed to for that are
  ;; duplicated in some of my general feeds. I just want them always
  ;; marked as read.
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :feed-url "planet.emacslife.com"
                                :entry-title (rx (or "Sacha Chua: "
                                                     "Irreal: "
                                                     "Protesilaos Stavrou: "
                                                     "Jeremy Friesen: "))
                                :remove 'unread))

  (of/global-definer
    "x" 'elfeed)

  (setq elfeed-feeds
        '(("http://www.pointer.io/rss/")
          ("https://stevelosh.com/rss.xml")
          ("http://planet.sbcl.org/rss20.xml" aggregate sbcl)
          ("https://planet.scheme.org/atom.xml" aggregate scheme)
          ("https://planet.lisp.org/rss20.xml" aggregate common-lisp)
          ("https://planet.emacslife.com/atom.xml" aggregate emacs)
          ("https://karl-voit.at/feeds/lazyblorg-all.atom_1.0.links-only.xml")
          ("https://irreal.org/blog/?feed=rss2")
          ("https://tilde.town/~ramin_hal9001/atom.xml")
          ("https://protesilaos.com/codelog.xml")
          ("https://karthinks.com/tags/emacs/index.xml" emacs)
          ("https://cestlaz.github.io/rss.xml" cestlaz)
          ("https://sachachua.com/blog/category/emacs/feed/" emacs)
          ("https://takeonrules.com/index.xml")
          ("https://www.masteringemacs.org/feed" emacs)
          ("https://nyxt.atlas.engineer/feed" nyxt)
          ("https://americanreformer.org/feed/" christianity)
          ("https://founders.org/feed/" christianity)
          ("https://christoverall.com/feed/" christianity)
          ("https://credomag.com/feed/" christianity theology)
          ("https://www.aomin.org/aoblog/feed/" christianity)
          ("https://www.aomin.org/aoblog/feed/?post_type=theologymatters" christianity)
          ("https://odysee.com/$/rss/@aominorg:0" video christianity)
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCRSbS2XhqOhnSUzWfGHUkLg" video christianity bible) ; Daily Dose of Greek
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCMldVrQ8rKkWyevvQonijoA" video christianity bible) ; Daily Dose of Latin
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UC0fRqEfY1ZaiWiJWTj7HQng" video christianity bible) ; Daily Dose of Hebrew
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCyl5V3-J_Bsy3x-EBCJwepg" video christianity comedy) ; Babylon Bee
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCgnQidqCmeyLEuXChuNEGmQ" video christianity) ; Canon Press
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCUMwY9iS8oMyWDYIe6_RmoA" video programming) ; No Boilerplate
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCAiiOTio8Yu69c3XnR7nQBQ" video programming) ; System Crafters
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCaSCt8s_4nfkRglWCvNSDrg" video programming) ; Code Aesthetic
          ("https://openrss.org/rumble.com/c/MattWalsh" video politics))))

(provide 'of-rss)
