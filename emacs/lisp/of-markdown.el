(require 'of-packages)

;; `goto-address-mode' interferes with Markdown's native link format,
;; causing some formatting issues. It's easiest to just turn it off.
(defun of/disable-goto-address-mode ()
  (goto-address-mode -1))

(use-package markdown-mode
  :hook (markdown-mode . of/disable-goto-address-mode))

(provide 'of-markdown)
