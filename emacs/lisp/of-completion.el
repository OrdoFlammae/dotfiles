(require 'of-packages)

(use-package vertico
  :config
  (vertico-mode 1))

(use-package company
  :config
  (global-company-mode)
  (add-to-list 'company-backends 'company-elisp)

  (define-key company-mode-map (kbd "C-M-i") #'company-complete))

(use-package marginalia
  :after vertico
  :config
  (setq marginalia-annotators '(marginalia-annotators-heavy
                                marginalia-annotators-light
                                nil))
  (marginalia-mode 1))

(use-package orderless
  :config
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles . (partial-completion))))))

(use-package which-key
  :config
  (which-key-mode 1))

(provide 'of-completion)
