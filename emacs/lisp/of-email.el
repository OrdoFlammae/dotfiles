(use-package mu4e
  :ensure nil
  :load-path "/usr/share/emacs/site-lisp/mu4e/"

  :config

  (setq mail-user-agent 'mu4e-user-agent)

  (setq mu4e-sent-messages-behavior 'delete)

  (add-to-list 'mu4e-bookmarks
               ;; ':favorite t' i.e, use this one for the modeline
               '(:query "maildir:/inbox" :name "Inbox" :key ?i :favorite t))

  ;; allow for updating mail using 'U' in the main view:
  (setq mu4e-get-mail-command "offlineimap")

  (setq mu4e-contexts
        (list
         (make-mu4e-context
          :name "personal"
          :enter-func (lambda ()
                        (mu4e-message "Entering personal context")
                        (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                          (revert-buffer)))
          :leave-func (lambda ()
                        (mu4e-message "Leaving personal context")
                        (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                          (revert-buffer)))
          :match-func (lambda (msg)
                        (when msg
                          (string-prefix-p "/personal" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "aidensaloyd@gmail.com")
                  (user-full-name . "Aiden Loyd")
                  (mu4e-drafts-folder . "/personal/[Gmail].Drafts")
                  (mu4e-sent-folder . "/personal/[Gmail].Sent Mail")
                  (mu4e-trash-folder . "/personal/[Gmail].Trash")
                  (mu4e-maildir-shortcuts . ((:maildir "/personal/INBOX"              :key ?i)
                                             (:maildir "/personal/[Gmail].Sent Mail"  :key ?s)
                                             (:maildir "/personal/[Gmail].Trash"      :key ?t :hide-unread t)
                                             (:maildir "/personal/[Gmail].All Mail"   :key ?a)))))
         (make-mu4e-context
          :name "ordoflammae"
          :enter-func (lambda ()
                        (mu4e-message "Entering ordoflammae context")
                        (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                          (revert-buffer)))
          :leave-func (lambda ()
                        (mu4e-message "Leaving ordoflammae context")
                        (when (string-match-p (buffer-name (current-buffer)) "mu4e-main")
                          (revert-buffer)))
          :match-func (lambda (msg)
                        (when msg
                          (string-prefix-p "/ordoflammae" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "aidensaloyd@gmail.com")
                  (user-full-name . "Aiden Loyd")
                  (mu4e-drafts-folder . "/ordoflammae/[Gmail].Drafts")
                  (mu4e-sent-folder . "/ordoflammae/[Gmail].Sent Mail")
                  (mu4e-trash-folder . "/ordoflammae/[Gmail].Trash")
                  (mu4e-maildir-shortcuts . ((:maildir "/ordoflammae/INBOX"              :key ?i)
                                             (:maildir "/ordoflammae/[Gmail].Sent Mail"  :key ?s)
                                             (:maildir "/ordoflammae/[Gmail].Trash"      :key ?t :hide-unread t)
                                             (:maildir "/ordoflammae/[Gmail].All Mail"   :key ?a)))))))

  (setq mu4e-context-policy 'pick-first))

(provide 'of-email)
