(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p custom-file)
    (load-file custom-file))

(setq backup-directory-alist '(("." . "~/.cache/emacs")))

;; Look for Nix-installed info files
(if (file-exists-p "~/.local/share/info/dir")
    (add-to-list 'Info-default-directory-list (expand-file-name "~/.local/share/info/")))

(setq native-comp-async-report-warnings-errors nil)

(require 'epg)
(setq epg-pinentry-mode 'loopback)

(setq vc-follow-symlinks t)

(when (eq system-type 'windows-nt)
  (define-key special-event-map (kbd "C-<lwindow>") 'ignore)
  (define-key special-event-map (kbd "C-<rwindow>") 'ignore))

(setq save-interprogram-paste-before-kill t)
(setq load-prefer-newer t)

(provide 'of-basic-settings)
