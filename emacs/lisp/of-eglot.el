(require 'of-packages)
(require 'of-general)

(defun of/eglot-get-language-id ()
  (require 'eglot)
  (let ((server (eglot-current-server)))
    (and (eglot-current-server)
         (cadr (eglot--lookup-mode major-mode)))))

(defun of/eglot--TextDocumentItem ()
  "Compute TextDocumentItem object for current buffer."
  (append
   (eglot--VersionedTextDocumentIdentifier)
   (list :languageId
         (of/eglot-get-language-id)
         :text
         (eglot--widening
          (buffer-substring-no-properties (point-min) (point-max))))))

(use-package eglot
  :ensure nil
  :after (general)
  :defer t
  :init
  (of/global-definer
    "e" '(:ignore t :which-key "eglot")
    "ec" 'eglot)

  :config
  (setq eglot-confirm-server-initiated-edits nil)

  ;; Override Language ID functionality
  (advice-add 'eglot--TextDocumentItem :override #'of/eglot--TextDocumentItem)
  (advice-add 'jsonrpc--message :override (lambda (&rest args) nil))

  (of/global-definer
    "ea" 'eglot-code-actions
    "er" 'eglot-reconnect
    "ek" 'eglot-shutdown
    "ef" 'eglot-format
    "en" 'eglot-rename
    "eb" '(:ignore t :which-key "buffer")))

(provide 'of-eglot)
