(provide 'of-c-cpp)

(with-eval-after-load 'cc-vars
  (setf (alist-get 'other c-default-style) "k&r"))

(add-hook 'c-mode-hook #'subword-mode)
(add-hook 'c-ts-mode-hook #'subword-mode)

(defvar *of/format* "clang-format")

(defun of/first-word (str)
  (substring str 0 (string-search " " str)))

(defun of/clang-format (begin end)
  (let* ((orig-windows (get-buffer-window-list (current-buffer)))
         (orig-window-starts (mapcar #'window-start orig-windows))
         (orig-point (point)))
    (unwind-protect
        (call-process-region (point-min) (point-max) *of/format*
                             t (list t nil) nil
                             "-offset" (number-to-string (1- begin))
                             "-length" (number-to-string (- end begin))
                             "-cursor" (number-to-string (1- (point)))
                             "-assume-filename" (buffer-file-name))
      (goto-char (point-min))
      (let ((json-output (json-read-from-string
                          (buffer-substring-no-properties
                           (point-min) (line-beginning-position 2)))))
        (delete-region (point-min) (line-beginning-position 2))
        (goto-char (1+ (cdr (assoc 'Cursor json-output))))
        (dotimes (index (length orig-windows))
          (set-window-start (nth index orig-windows)
                            (nth index orig-window-starts)))))))

(defun of/format-buffer ()
  (interactive ""
               'c-mode
               'c-ts-mode
               'c++-mode
               'c++-ts-mode)

  (when (not (buffer-file-name))
    (error "A buffer must be associated with a file in order to use REFORMAT-REGION."))

  (let ((prog-name (of/first-word *of/format*)))
    (when (not (executable-find prog-name))
      (error "%s not found." prog-name)))

  (when (locate-dominating-file (buffer-file-name) ".clang-format")
    (of/clang-format (point-min) (point-max)))

  (save-mark-and-excursion
    (indent-region (point-min) (point-max))))

(defun of/format-buffer-on-save ()
  (add-hook 'before-save-hook #'of/format-buffer nil t))

(add-hook 'c-mode-hook #'of/format-buffer-on-save)
(add-hook 'c-ts-mode-hook #'of/format-buffer-on-save)

(setq gdb-debuginfod-enable-setting nil)
