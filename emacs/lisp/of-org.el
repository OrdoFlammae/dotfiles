(require 'of-packages)
(require 'of-general)

(defun of/org-mode-setup ()
  (org-indent-mode)
  (visual-line-mode 1)
  (variable-pitch-mode 1))

(use-package org
  :after general
  :config
  (add-hook 'org-mode-hook #'of/org-mode-setup)

  (setq org-link-frame-setup '((vm . vm-visit-folder)
                               (vm-imap . vm-visit-imap-folder-other-frame)
                               (gnus . org-gnus-no-new-news)
                               (file . find-file)
                               (wl . wl))
        org-archive-location "~/org/.archive.org::* From %s")

  (setq org-capture-templates
        `(("t" "todo" entry (file "~/org/gtd/inbox.org")
           "* TODO %?\n%U\n%a\n")
          ("n" "note" entry (file "~/org/gtd/inbox.org")
           "* %? :NOTE:\n%U\n%a\n")
          ("m" "meeting" entry (file "~/org/gtd/inbox.org")
           "* MEETING with %? :MEETING:\n%U")))

  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
          (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "MEETING"))
        org-todo-keyword-faces
        '(("TODO" :foreground "red" :weight bold)
          ("NEXT" :foreground "blue" :weight bold)
          ("DONE" :foreground "forest green" :weight bold)
          ("WAITING" :foreground "orange" :weight bold)
          ("HOLD" :foreground "magenta" :weight bold)
          ("CANCELLED" :foreground "forest green" :weight bold)
          ("MEETING" :foreground "forest green" :weight bold))
        org-use-fast-todo-selection t
        org-treat-S-cursor-todo-selection-as-state-change nil
        org-log-into-drawer t)

  (setq org-agenda-files '("~/org/roam/" "~/org/gtd/")
        org-agenda-span 1
        org-agenda-start-on-weekday 0
        org-agenda-skip-deadline-prewarning-if-scheduled t
        org-agenda-todo-ignore-with-date t)

  (setq org-agenda-prefix-format '((agenda . " %i %-15:c%?-12t% s")
                                   (todo . " %i %-12:c")
                                   (tags . " %i %-12:c")
                                   (search . " %i %-12:c")))

  (setq org-refile-targets '((nil :maxlevel . 10)
                             (org-agenda-files :maxlevel . 10))
        org-refile-use-outline-path 'file
        org-outline-path-complete-in-steps nil
        org-refile-allow-creating-parent-nodes 'confirm)

  ;; Make bullet points look a bit nicer.
  (font-lock-add-keywords 'org-mode
                        '(("^ *\\([-+]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (of/global-definer
    "a" 'org-agenda
    "c" 'org-capture))

(use-package org-faces
  :ensure nil
  :after org
  :config
  (require 'org-indent)

  ;; Increase the size of various headings
  ;; (set-face-attribute 'org-document-title nil :font "Iosevka Aile" :weight 'bold :height 1.3)

  ;; (dolist (face '((org-level-1 . 1.2)
  ;;                 (org-level-2 . 1.1)
  ;;                 (org-level-3 . 1.05)
  ;;                 (org-level-4 . 1.0)
  ;;                 (org-level-5 . 1.0)
  ;;                 (org-level-6 . 1.0)
  ;;                 (org-level-7 . 1.0)
  ;;                 (org-level-8 . 1.0)))
  ;;   (set-face-attribute (car face) nil :font "Iosevka Aile" :weight 'medium :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-block-begin-line nil :background 'unspecified :inherit 'fixed-pitch)
  (set-face-attribute 'org-block-end-line nil :background 'unspecified :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

  ;; Get rid of the background on column views
  (set-face-attribute 'org-column nil :background 'unspecified)
  (set-face-attribute 'org-column-title nil :background 'unspecified))

;; TODO: Others to consider
;; '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
;; '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;; '(org-property-value ((t (:inherit fixed-pitch))) t)
;; '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;; '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
;; '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
;; '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))

(use-package org-tempo
  :ensure nil
  :after org
  :config
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp")))

(defun of/insert-created-timestamp (&rest args)
  "Insert a CREATED property using org-expiry.el for TODO entries"
  (org-expiry-insert-created)
  (org-back-to-heading)
  (org-end-of-line))

(use-package org-contrib
  :after org)

(use-package org-expiry
  :ensure nil
  :after org-contrib
  :config
  (setq org-expiry-created-property-name "CREATED"
        org-expiry-inactive-timestamps t)

  (advice-add 'org-insert-todo-heading :after #'of/insert-created-timestamp))

(use-package org-make-toc
  :hook (org-mode . org-make-toc-mode))

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(use-package org-roam
  :after (org general)
  :config
  (setq org-roam-directory (file-name-concat org-directory "roam"))
  (if (not (file-exists-p org-roam-directory))
      (make-directory org-roam-directory t))
  (org-roam-db-autosync-mode)

  (of/global-definer
    "n" '(:ignore t :which-key "org-roam")
    "nf" 'org-roam-node-find
    "nr" 'org-roam-node-random

    "ni" 'org-roam-node-insert
    "no" 'org-id-get-create
    "nt" 'org-roam-tag-add
    "na" 'org-roam-alias-add
    "nl" 'org-roam-buffer-toggle

    "nd" '(:keymap org-roam-dailies-map :package org-roam-dailies :which-key "org-roam-dailies")))

(use-package org-ref
  :after org
  :config
  (setq org-latex-pdf-process '("%latex -interaction nonstopmode -output-directory %o %f" "biber %b" "%latex -interaction nonstopmode -output-directory %o %f" "%latex -interaction nonstopmode -output-directory %o %f")))

(use-package org-drill
  :after (org general)
  :config
  (of/global-definer
    "d" '(:ignore t :which-key "org-drill")
    "dd" 'org-drill
    "dt" 'org-drill-tree
    "dc" 'org-drill-cram-tree))

(use-package ox-reveal
  :after (org))

(provide 'of-org)
