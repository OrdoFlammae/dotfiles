(require 'of-packages)

;;; Elisp
(define-key emacs-lisp-mode-map (kbd "C-c C-m") 'emacs-lisp-macroexpand)

;;; Common Lisp
(use-package sly
  :defer t
  :commands (sly)
  :config
  (if (executable-find "ros")
      (setq inferior-lisp-program "ros -Q run")
    (setq inferior-lisp-program "sbcl")))

(use-package sly-overlay
  :after (sly)
  :config
  (define-key sly-mode-map [remap sly-eval-defun] 'sly-overlay-eval-defun))

(modify-syntax-entry ?| "$" lisp-mode-syntax-table)

;;; Racket (technically scheme, but doesn't use Geiser)
(use-package racket-mode
  :defer t)

(use-package smartparens
  :hook ((smartparens-mode . smartparens-strict-mode)
         ((lisp-mode
           lisp-data-mode
           inferior-lisp-mode
           inferior-emacs-lisp-mode
           geiser-mode)
          . smartparens-strict-mode)) ;; This is very hard to read, is
                                      ;; there a better way?
  :config
  (require 'smartparens-config)

  (setq sp-highlight-pair-overlay nil)

  (sp-use-smartparens-bindings)
  (define-key smartparens-mode-map (kbd "M-<delete>") #'sp-kill-word)
  (define-key smartparens-mode-map (kbd "M-<backspace>") #'sp-backward-kill-word)

  (define-key smartparens-mode-map (kbd "C-<delete>") #'sp-unwrap-sexp)
  (define-key smartparens-mode-map (kbd "C-<backspace>") #'sp-backward-unwrap-sexp))

(require 'project-mage-abbrevs)

(provide 'of-lisp)
