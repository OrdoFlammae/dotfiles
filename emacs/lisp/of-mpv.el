(require 'of-packages)
(require 'of-general)

(defvar *of/video-dir* "~/videos/")

(use-package mpv
  :commands (mpv-play)
  :init

  :config
  (defun of/mpv-is-active ()
    (not (null mpv--process)))

  (defun of/mpv-playlist-append-list (thing)
    (mpv-run-command "loadlist" thing "append")
    (when-let* ((count (mpv-get-property "playlist-count"))
                (index (1- count))
                (filename (mpv-get-property (format "playlist/%d/filename" index))))
      (message "Added `%s' to the current playlist" filename)))

  (defun of/mpv-append-if-active (mpv-start thing &rest args)
    (if (of/mpv-is-active)
        (mpv--playlist-append thing)
      (let ((default-directory (expand-file-name *of/video-dir*)))
        (apply mpv-start thing args))))

  (advice-add 'mpv-start :around #'of/mpv-append-if-active)

  ;; Use MPV as the default method to open youtube URLs
  ;;
  ;; I need this function to ignore the extra parameters that are sent
  ;; by `browse-url'
  (defun of/mpv-play-url (url &rest args)
    (mpv-play-url url))

  (with-eval-after-load 'elfeed
    (add-to-list 'browse-url-handlers
                 (cons (rx (or "youtube.com/watch?v="
                               "rumble.com/"
                               "odysee.com/"))
                       #'of/mpv-play-url)))
  ;; Start MPV paused
  (setq mpv-default-options '("--pause")
        mpv-start-timeout 2))

(use-package youtube-dl
  :ensure (youtube-dl :host github :repo "poretsky/youtube-dl-emacs")
  :init
  (require 'youtube-dl)

  :config
  (setf youtube-dl-immediate 'always)
  (setf youtube-dl-download-directory "~/downloads/youtube")

  (defun of/after-downloading-item (item)
    (let* ((file-data (car (youtube-dl--file-lists (list item))))
           (default-directory (car file-list))
           (file-list (cl-remove-if (lambda (file)
                                      (s-ends-with-p ".part" file))
                                    (cdr file-data))))
      (if (cdr file-list)
          (warn "Found multiple files associated with item. Added all to playlist"))
      (cl-mapc (lambda (file)
                 (if (not (file-exists-p file))
                     (error "Tried to play nonexistent file! %s" file)
                   (mpv-play file)))
               file-list)))

  (advice-add 'youtube-dl--remove :after 'of/after-downloading-item))

(provide 'of-mpv)
