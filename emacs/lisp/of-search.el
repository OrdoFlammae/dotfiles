(setq isearch-lazy-count t)
(setq lazy-count-prefix-format nil)
(setq lazy-count-suffix-format "   (%s/%s)")

(use-package wgrep
  :hook (grep-mode . wgrep-setup))

(use-package rg
  :config
  (rg-enable-default-bindings))

(use-package consult
  :bind (("C-M-l" . consult-imenu)))

(provide 'of-search)
