(require 'of-packages)

(use-package general
  :config
  (general-create-definer of/global-definer
    :prefix "C-c")

  (of/global-definer
    "f" '(:prefix-command open-file-map :which-key "open file")
    "fc" '(:prefix-command configuration-file-map :which-key "configuration")
    "fce" '((lambda ()
              "Go to Emacs Configuration file"
              (interactive)
              (find-file (file-name-concat *of-dotfiles-dir* "init.el")))
            :which-key "emacs configuration")

    "u" '(:prefix-command os-util-map :which-key "os utils")))

(elpaca-wait)

(provide 'of-general)
