(let* ((opam-share (ignore-errors (car (process-lines "opam" "var" "share"))))
       (opam-elisp (and opam-share (expand-file-name "emacs/site-lisp" opam-share))))
  (when (and opam-elisp (file-directory-p opam-elisp))
    (add-to-list 'load-path opam-elisp)

    (loaddefs-generate opam-elisp (expand-file-name "opam-autoloads.el" opam-elisp))
    (load "opam-autoloads")

    ;; Automatically start Merlin in OCaml buffers
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)
    ;; Use opam switch to lookup ocamlmerlin binary
    (setq merlin-command 'opam)
    ;; To easily change opam switches within a given Emacs session, you can
    ;; install the minor mode https://github.com/ProofGeneral/opam-switch-mode
    ;; and use one of its "OPSW" menus.

    ;; Enable Utop bindings in Tuareg
    (add-hook 'tuareg-mode-hook 'utop-minor-mode)

    (add-hook 'dune-mode-hook 'smartparens-mode)

    (require 'ocp-indent)))

(provide 'of-ocaml)
