(require 'of-general)

(use-package ace-window
  :after general
  :config
  (global-set-key (kbd "M-S-o") 'ace-window))

(provide 'of-navigation)
