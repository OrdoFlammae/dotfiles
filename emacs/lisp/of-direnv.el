(require 'of-packages)

(use-package direnv
  :if (executable-find "direnv")
  :config
  (direnv-mode))

(provide 'of-direnv)
