(require 'of-packages)

;; A small extension to distinguish between JSX and TSX for the
;; benefit of Eglot
(define-derived-mode jsx-ts-mode tsx-ts-mode
  "JavaScript[JSX]")

;; This is to patch over a minor issue in `company' where it thinks
;; that a delimiter has been reached when completing string
;; constants. Need to find a better solution to this problem in the
;; future.
(defun of/fix-js-syntax-tables ()
  (modify-syntax-entry ?: "w"))

(mapc (lambda (mode)
        (add-hook mode #'of/fix-js-syntax-tables))
      '(js-base-mode-hook typescript-ts-base-mode-hook))

;; Need to load afterwards, otherwise `treesit-auto' overwrites the
;; mode for JSX
(eval-after-load 'treesit-auto
  (lambda ()
    (add-to-list 'auto-mode-alist '("\\.js\\'" . js-ts-mode))
    (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode))
    (add-to-list 'auto-mode-alist '("\\.jsx\\'" . jsx-ts-mode))
    (add-to-list 'auto-mode-alist '("\\.tsx\\'" . tsx-ts-mode))))

(defun of/run-if-prettier-disabled (f &rest args)
  (if (not prettier-mode)
      (apply f args)))

(defun of/disable-whitespace-cleanup ()
  (advice-add 'whitespace-cleanup :around #'of/run-if-prettier-disabled))

(use-package prettier
  :config
  (add-to-list 'prettier-major-mode-parsers '(js-base-mode . prettier--guess-js-ish))
  (add-to-list 'prettier-major-mode-parsers '(jsx-ts-mode . prettier--guess-js-ish))
  (add-to-list 'prettier-major-mode-parsers '(typescript-ts-base-mode . (typescript babel-ts)))
  (add-to-list 'prettier-major-mode-parsers
               '(json-ts-mode . (lambda nil
                                  (if
                                      (and buffer-file-name
                                           (seq-contains
                                            '("package.json" "package-lock.json" "composer.json")
                                            (file-name-nondirectory buffer-file-name)))
                                      '(json-stringify json json5)
                                    '(json json5 json-stringify)))))

  (global-prettier-mode)

  (add-hook 'prettier-mode-hook #'of/disable-whitespace-cleanup))

(defun of/epithet-for-javascript ()
  (when (and (derived-mode-p 'js-base-mode 'typescript-ts-base-mode)
             (string-match-p (rx string-start
                                 "index"
                                 (or ".js" ".jsx" ".ts" ".tsx")
                                 string-end)
                             (buffer-name)))
    (format "%s/%s"
            (file-name-nondirectory
             (directory-file-name
              (file-name-directory
               (buffer-file-name))))
            (buffer-name))))

(use-package epithet
  :ensure (:host github :repo "oantolin/epithet")
  :config
  ;; Remove all the default epithet suggesters
  (remove-hook 'epithet-suggesters #'epithet-for-shell-command)
  (remove-hook 'epithet-suggesters #'epithet-for-Info)
  (remove-hook 'epithet-suggesters #'epithet-for-eww-title)
  (remove-hook 'epithet-suggesters #'epithet-for-eww-url)
  (remove-hook 'epithet-suggesters #'epithet-for-help)
  (remove-hook 'epithet-suggesters #'epithet-for-occur)
  (remove-hook 'epithet-suggesters #'epithet-for-compilation)

  (add-hook 'epithet-suggesters #'of/epithet-for-javascript))

(defun of/setup-ts-eglot ()
  (when (derived-mode-p 'js-base-mode 'typescript-ts-base-mode)
    (eglot--code-action eglot-code-action-organize-imports "source.organizeImports.ts")))

(eval-after-load 'eglot
  (lambda ()
    (add-hook 'eglot-managed-mode-hook #'of/setup-ts-eglot)

    ;; Eglot's default `languageId' configurations for JS and related
    ;; languages is pretty terrible
    (defun of/eglot-programs-contains-mode (programs mode)
      (and (listp programs)
           (member mode programs)))

    ;; First, I need to clear out the cached server programs for
    ;; `js-mode', along with other possibly related modes.

    (setq eglot-server-programs
          (assoc-delete-all 'js-mode eglot-server-programs
                            #'of/eglot-programs-contains-mode))

    (add-to-list 'eglot-server-programs
                 (cons '((js-ts-mode :language-id "javascript")
                         (jsx-ts-mode :language-id "javascriptreact")
                         (typescript-ts-mode :language-id "typescript")
                         (tsx-ts-mode :language-id "typescriptreact"))
                       '("npx" "typescript-language-server" "--stdio")))

    (add-to-list 'eglot-server-programs
                 `(json-ts-mode . ,(eglot-alternatives
                                    '(("vscode-json-language-server" "--stdio")
                                      ("vscode-json-languageserver" "--stdio")
                                      ("json-languageserver" "--stdio")))))))

(add-to-list 'auto-mode-alist `("\\.gyp\\'" . json-ts-mode))

(with-eval-after-load 'rg
  (add-to-list 'rg-custom-type-aliases '("webscript" . "*.js *.jsx *.ts *.tsx")))

(provide 'of-js-ts)
