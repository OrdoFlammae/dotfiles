(require 'of-packages)
(require 'of-general)

(of/global-definer
  "," '(:prefix-command edit-map :which-key "edit")
  ",c" 'compile
  ",r" 'recompile)

(require 'of-packages)

(use-package rainbow-identifiers
  :hook (prog-mode . rainbow-identifiers-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(require 'of-direnv)
(require 'of-yasnippet)
(require 'of-tree-sitter)
(require 'of-eglot)
(require 'of-flymake)
(require 'of-magit)
(require 'of-eshell)
(require 'of-projects)

(require 'of-generic-modes)

;; Languages

(require 'of-c-cpp)
(require 'of-java)
(require 'of-js-ts)
(require 'of-php)
(require 'of-lisp)
(require 'of-haskell)
(require 'of-rust)
(require 'of-dart)

(provide 'of-programming)
