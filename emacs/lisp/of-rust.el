(use-package rustic
  :config
  (setq rustic-lsp-client 'eglot))

(provide 'of-rust)
