;; https://www.emacswiki.org/emacs/AbbrevMode
;; note that abbreviations must be lower-case and non-punctuation, if you add
;; your own
(define-abbrev-table
  'global-abbrev-table
  '(("xi" "Ξ")
    ("gv" "←")
    ("sv" "→")
    ("uu" "↑")
    ("dd" "↓")
    ("ddd" "↓↓")
    ("ld" "λ")
    ("0vec" "0ᵥ")
    ("1vec" "1ᵥ")
    ("xunit" "x̂")
    ("yunit" "ŷ")))

(add-hook 'lisp-mode-hook 'abbrev-mode)
(add-hook 'text-mode-hook 'abbrev-mode)
(add-hook 'minibuffer-mode-hook 'abbrev-mode)

(global-set-key (kbd "M-1") 'expand-abbrev) ; for other modes

(defun try-mage-readtable ()
  "Find if the mage readtable is declared in the file and eval it."
  (when (string-match (rx "in-standard-readtable)")
                      (buffer-string))
    (sly-interactive-eval "(mage.utils:in-standard-readtable)")))

(defadvice sly-eval-region (before readtable activate)
  (try-mage-readtable))

(defadvice sly-compile-region (before readtable activate)
  (try-mage-readtable))

(defadvice sly-macroexpand-1 (before readtable activate)
  (try-mage-readtable))

(defadvice sly-macroexpand-all (before readtable activate)
  (try-mage-readtable))

(defadvice sly-expand-1 (before readtable activate)
  (try-mage-readtable))

(defun sly-test-local-package ()
  (interactive)
  (sly-interactive-eval "(parachute:test cl:*package*)"))

(provide 'project-mage-abbrevs)
