(global-set-key (kbd "M-o") #'other-window)
(global-set-key (kbd "C-x C-v") #'revert-buffer-quick)

(global-set-key (kbd "M-g r") #'async-shell-command)

(provide 'of-keys)
