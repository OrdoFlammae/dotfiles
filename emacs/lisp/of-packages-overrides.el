;; There are some packages that come with Emacs by default, but need to be overriden to work correctly.

(defun +elpaca-unload-seq (e)
  (and (featurep 'seq) (unload-feature 'seq t))
  (elpaca--continue-build e))

(defun +elpaca-seq-build-steps ()
  (append (butlast (if (file-exists-p (expand-file-name "seq" elpaca-builds-directory))
                       elpaca--pre-built-steps elpaca-build-steps))
          (list '+elpaca-unload-seq 'elpaca--activate-package)))

;; Need to upgrade the version of `seq' for `transient' to build
;; correctly (one of `magit''s dependencies)
(use-package seq
  :ensure `(seq :build ,(+elpaca-seq-build-steps)))

(defun +elpaca-unload-transient (e)
  (and (featurep 'transient) (unload-feature 'transient t))
  (elpaca--continue-build e))

(defun +elpaca-transient-build-steps ()
  (append (butlast (if (file-exists-p (expand-file-name "transient" elpaca-builds-directory))
                       elpaca--pre-built-steps elpaca-build-steps))
          (list '+elpaca-unload-transient 'elpaca--activate-package)))

(use-package transient
  :ensure `(transient :build ,(+elpaca-transient-build-steps)))

(elpaca-wait)

(provide 'of-packages-overrides)
