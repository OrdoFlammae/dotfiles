(require 'of-packages)

(use-package haskell-mode
  :defer t
  :init
  (setq haskell-stylish-on-save t))

(provide 'of-haskell)
