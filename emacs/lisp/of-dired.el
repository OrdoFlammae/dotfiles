(global-set-key (kbd "C-x C-d") #'dired)
(setq dired-listing-switches "-alh")

(require 'of-packages)

(use-package dired-du
  :init
  (setq dired-du-size-format t))

(provide 'of-dired)
