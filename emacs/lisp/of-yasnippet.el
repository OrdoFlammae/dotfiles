(require 'of-packages)

(use-package yasnippet
  :config
  (add-to-list 'yas-snippet-dirs (file-name-concat *of-dotfiles-dir* "snippets"))
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :after yasnippet)

(provide 'of-yasnippet)
