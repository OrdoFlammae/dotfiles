;; Makes links globally clickable
(global-goto-address-mode 1)

(require 'of-documents)

(require 'of-rss)
(require 'of-mpv)
(require 'of-eshell)
(require 'of-dired)
(require 'of-vterm)

(provide 'of-productivity)
