(global-set-key (kbd "C-x m") #'eshell)

(defun of/add-to-eshell-path (path)
  "Add a folder to Eshell path if it is a proper name and it exists"
  (let ((path (expand-file-name path)))
    (when (file-exists-p path)
      (eshell/addpath path))))

(defun of/configure-eshell-path ()
  (of/add-to-eshell-path "~/go/bin"))

(add-hook 'eshell-mode-hook #'of/configure-eshell-path)

(provide 'of-eshell)
