(defvar *of-dotfiles-dir*
  (file-name-parent-directory
   (file-truename
    (file-name-concat user-emacs-directory "init.el"))))

(add-to-list 'load-path (file-name-concat *of-dotfiles-dir* "lisp/"))

(require 'of-basic-settings)
(require 'of-edit-settings)
(require 'of-visual-settings)
(require 'of-keys)
(require 'of-productivity)
(require 'of-completion)
(require 'of-navigation)
(require 'of-search)
(require 'of-programming)
